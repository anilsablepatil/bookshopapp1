package com.tw.bookshop.unit.controller;

import com.tw.bookshop.controller.BookController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BookControllerTest {
    private BookController bookController;
    @Before
    public void setup(){
        bookController = new BookController();
    }

    @Test
    public void shouldReturnBookShopName(){
        Assert.assertEquals("My Book Shop", bookController.getBookShopName().getName());
    }

}
