package com.tw.bookshop.integration.controller;


import com.tw.bookshop.controller.BookController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookController.class, secure = false)
public class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mock;

    @Test
    public void testBookShopName() throws Exception {
        Assert.assertEquals("{\"name\":\"My Book Shop\"}",
        mock.perform(
                MockMvcRequestBuilders.get("/applications")).andReturn().getResponse().getContentAsString());
    }

}
