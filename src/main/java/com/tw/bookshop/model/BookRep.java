package com.tw.bookshop.model;

public class BookRep {
    private String name;

    public BookRep(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
