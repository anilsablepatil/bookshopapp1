package com.tw.bookshop.controller;

import com.tw.bookshop.model.BookRep;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {
    @GetMapping("/applications")
    public BookRep getBookShopName(){
        return new BookRep("My Book Shop");
    }
}
