FROM openjdk:8-alpine
ADD /build/libs/bookshop*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]